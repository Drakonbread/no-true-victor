# No True Victor

An alternate-history mod for the grand-strategy game HOI4 that takes place in a universe where the gruelling fighting in WW1 wasn't won by anyone and the world plunged into slight chaos.